//
// Created by ismael on 11/05/18.
//

#include <iostream>
#include "board.h"

using namespace std;

board::board() {
    for (int i = 0; i < HEIGHT; i++)
        for (int j = 0; j < WIDTH; j++)
            this->myBoard[i][j] = i+j;
}

board::~board() {

}

const void board::displayBoard() {
    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++)
            cout << myBoard[i][j];
        cout << endl;
    }
    return;
}
