//
// Created by ismael on 11/05/18.
//

#ifndef TEST2048_BOARD_H
#define TEST2048_BOARD_H

#define WIDTH 10
#define HEIGHT 20

class board {
    int myBoard[HEIGHT][WIDTH];
public:
    board();

    virtual ~board();

    const void displayBoard();
};


#endif //TEST2048_BOARD_H
