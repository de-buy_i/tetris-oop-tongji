//
// Created by ismael on 12/05/18.
//

#include <iostream>
#include <random>
#include "tetrimino.h"

tetrimino::tetrimino()
{
    this->Tetriminos[0]='I';
    this->Tetriminos[1]='O';
    this->Tetriminos[2]='T';
    this->Tetriminos[3]='S';
    this->Tetriminos[4]='Z';
    this->Tetriminos[5]='J';
    this->Tetriminos[6]='L';
}

tetrimino::~tetrimino() {

}

char    tetrimino::getPiece()
{
    std::random_device rd;
    return (this->Tetriminos[(rd()%6)]);
}
