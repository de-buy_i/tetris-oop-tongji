//
// Created by ismael on 12/05/18.
//

#ifndef TEST2048_TETRIMINO_H
#define TEST2048_TETRIMINO_H

#include <map>

class tetrimino {
    std::map<int , char> Tetriminos;
public:
    tetrimino();

    virtual ~tetrimino();

    char getPiece();
};


#endif //TEST2048_TETRIMINO_H
